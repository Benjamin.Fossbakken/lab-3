package no.uib.inf101.terminal;


public class CmdEcho implements Command {
    
    @Override
    public String getName(){
        return ("echo");
    }

    @Override
    public String run(String[] args){
        String p = "";
        for (String string : args) {
            p += (string + " ");
        }
        return (p);
    }
}

    
    
    

